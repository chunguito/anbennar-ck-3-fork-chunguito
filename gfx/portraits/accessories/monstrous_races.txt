﻿#tail_01 = {
#	entity = {	    node = "body_root"	entity = "tail_entity" }
#}
#
kobold_tail_01 = {
	entity = {	    node = "body_root"	entity = "kobold_tail_entity" }
}
#########################################################################################################################

male_kobold_horns_straight = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_horns_straight_entity		 }
}

male_kobold_horns_curved = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_horns_curved_entity		 }
}

male_kobold_horns_goat = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_horns_goat_entity		 }
}

female_kobold_horns_straight = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_horns_straight_entity		 }
}

female_kobold_horns_curved = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_horns_curved_entity		 }
}

female_kobold_horns_goat = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_horns_goat_entity		 }
}


#########################################################################################################################

male_kobold_jaw_spikes_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_A_entity		 }
}

female_kobold_jaw_spikes_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_A_entity		 }
}

#########################################################################################################################

male_kobold_jaw_spikes_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_B_entity		 }
}

female_kobold_jaw_spikes_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_B_entity		 }
}

#########################################################################################################################

male_kobold_jaw_spikes_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_C_entity		 }
}

female_kobold_jaw_spikes_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_C_entity		 }
}

#########################################################################################################################

male_kobold_jaw_spikes_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_D_entity		 }
}

female_kobold_jaw_spikes_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_D_entity		 }
}

#########################################################################################################################

male_kobold_jaw_spikes_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_E_entity		 }
}

female_kobold_jaw_spikes_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_E_entity		 }
}

#########################################################################################################################

male_kobold_jaw_spikes_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_F_entity		 }
}

female_kobold_jaw_spikes_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_F_entity		 }
}

#########################################################################################################################

male_kobold_jaw_spikes_G = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_jaw_spikes_G_entity		 }
}

female_kobold_jaw_spikes_G = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_jaw_spikes_G_entity		 }
}

#########################################################################################################################

male_kobold_cheek_spikes_straight_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_straight_A_entity		 }
}

female_kobold_cheek_spikes_straight_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_straight_A_entity		 }
}

male_kobold_cheek_spikes_curved_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_curved_A_entity		 }
}

female_kobold_cheek_spikes_curved_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_curved_A_entity		 }
}

#########################################################################################################################

male_kobold_cheek_spikes_straight_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_straight_B_entity		 }
}

female_kobold_cheek_spikes_straight_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_straight_B_entity		 }
}

male_kobold_cheek_spikes_curved_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_curved_B_entity		 }
}

female_kobold_cheek_spikes_curved_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_curved_B_entity		 }
}

#########################################################################################################################

male_kobold_cheek_spikes_straight_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_straight_C_entity		 }
}

female_kobold_cheek_spikes_straight_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_straight_C_entity		 }
}

male_kobold_cheek_spikes_curved_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_curved_C_entity		 }
}

female_kobold_cheek_spikes_curved_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_curved_C_entity		 }
}

#########################################################################################################################

male_kobold_cheek_spikes_straight_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_straight_D_entity		 }
}

female_kobold_cheek_spikes_straight_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_straight_D_entity		 }
}

male_kobold_cheek_spikes_curved_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_curved_D_entity		 }
}

female_kobold_cheek_spikes_curved_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_curved_D_entity		 }
}

#########################################################################################################################

male_kobold_cheek_spikes_straight_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_straight_E_entity		 }
}

female_kobold_cheek_spikes_straight_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_straight_E_entity		 }
}

male_kobold_cheek_spikes_curved_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_curved_E_entity		 }
}

female_kobold_cheek_spikes_curved_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_curved_E_entity		 }
}

#########################################################################################################################

male_kobold_cheek_spikes_straight_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_straight_F_entity		 }
}

female_kobold_cheek_spikes_straight_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_straight_F_entity		 }
}

male_kobold_cheek_spikes_curved_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_cheek_spikes_curved_F_entity		 }
}

female_kobold_cheek_spikes_curved_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_cheek_spikes_curved_F_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_A_entity		 }
}

female_kobold_eyebrow_spikes_straight_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_A_entity		 }
}

male_kobold_eyebrow_spikes_curved_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_A_entity		 }
}

female_kobold_eyebrow_spikes_curved_A = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_A_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_B_entity		 }
}

female_kobold_eyebrow_spikes_straight_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_B_entity		 }
}

male_kobold_eyebrow_spikes_curved_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_B_entity		 }
}

female_kobold_eyebrow_spikes_curved_B = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_B_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_C_entity		 }
}

female_kobold_eyebrow_spikes_straight_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_C_entity		 }
}

male_kobold_eyebrow_spikes_curved_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_C_entity		 }
}

female_kobold_eyebrow_spikes_curved_C = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_C_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_D_entity		 }
}

female_kobold_eyebrow_spikes_straight_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_D_entity		 }
}

male_kobold_eyebrow_spikes_curved_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_D_entity		 }
}

female_kobold_eyebrow_spikes_curved_D = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_D_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_E_entity		 }
}

female_kobold_eyebrow_spikes_straight_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_E_entity		 }
}

male_kobold_eyebrow_spikes_curved_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_E_entity		 }
}

female_kobold_eyebrow_spikes_curved_E = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_E_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_F_entity		 }
}

female_kobold_eyebrow_spikes_straight_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_F_entity		 }
}

male_kobold_eyebrow_spikes_curved_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_F_entity		 }
}

female_kobold_eyebrow_spikes_curved_F = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_F_entity		 }
}

#########################################################################################################################

male_kobold_eyebrow_spikes_straight_G = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_straight_G_entity		 }
}

female_kobold_eyebrow_spikes_straight_G = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_straight_G_entity		 }
}

male_kobold_eyebrow_spikes_curved_G = {
	entity = { required_tags = "" shared_pose_entity = head  entity = male_kobold_eyebrow_spikes_curved_G_entity		 }
}

female_kobold_eyebrow_spikes_curved_G = {
	entity = { required_tags = "" shared_pose_entity = head  entity = female_kobold_eyebrow_spikes_curved_G_entity		 }
}